var gulp = require('gulp');
var gulpHelpers = require('gulp-helpers');
var requirejsOptimize = require('gulp-requirejs-optimize');
var concat = require('gulp-concat');
var minifyCss = require('gulp-minify-css');
var preprocess = require('gulp-preprocess');

var paths = {
    js: 'www/app/',
    css: 'www/assets/css/',
    libs: 'www/assets/libs/'
};

gulp.task('compress-js', function () {
    return gulp.src(paths.js + 'main.js')
        .pipe(requirejsOptimize(function(file) {
            return {
                mainConfigFile: paths.js + 'main.js'
            };
        }))
        .pipe(concat('index.js'))
        .pipe(gulp.dest('www/'));
});

gulp.task('minify-css', function () {
    return gulp.src([paths.css + 'reset.css', paths.libs + 'angular-material/angular-material.css', paths.libs + 'fullcalendar/dist/fullcalendar.min.css', paths.css + 'app.css'])
        .pipe(minifyCss())
        .pipe(concat('index.css'))
        .pipe(gulp.dest('www/'));
});

gulp.task('build', ['compress-js','minify-css'], function () {});

gulp.task('production', function () {
    return gulp.src('www/tmpl/index.html')
        .pipe(preprocess({context: { NODE_ENV: 'production'}}))
        .pipe(gulp.dest('./www/'));
});

gulp.task('develop', function () {
    return gulp.src('www/tmpl/index.html')
        .pipe(preprocess({context: { NODE_ENV: 'develop'}}))
        .pipe(gulp.dest('./www/'));
});