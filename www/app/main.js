require.config({

    paths: {
        'domReady': '../assets/libs/requirejs-domready/domReady',
        'jquery': '../assets/libs/jquery/dist/jquery.min',
        'moment': '../assets/libs/momentjs/min/moment.min',
        'fullcalendar': '../assets/libs/fullcalendar/dist/fullcalendar.min',
        '_': '../assets/libs/underscore/underscore-min',

        'angular': '../assets/libs/angular/angular.min',
        'angular_animate': '../assets/libs/angular-animate/angular-animate.min',
        'angular_aria': '../assets/libs/angular-aria/angular-aria.min',
        'angular_material': '../assets/libs/angular-material/angular-material.min',
        'angular_messages': '../assets/libs/angular-messages/angular-messages.min',
        'angular_route': '../assets/libs/angular-route/angular-route.min',
        'angular_resource': '../assets/libs/angular-resource/angular-resource.min',
        'angular_ui_router': '../assets/libs/angular-ui-router/release/angular-ui-router.min',
        'angular_timer': '../assets/libs/angular-timer/dist/angular-timer.min'
    },

    shim: {
        'angular': {
            exports: 'angular'
        },
        'angular_aria': {
            deps: ['angular'],
            exports: 'angular_aria'
        },
        'angular_animate': {
            deps: ['angular'],
            exports: 'angular_animate'
        },
        'angular_material': {
            deps: ['angular','angular_aria','angular_animate','angular_messages'],
            exports: 'angular_material'
        },
        'angular_messages': {
            deps: ['angular'],
            exports: 'angular_messages'
        },
        'angular_resource': {
            deps: ['angular'],
            exports: 'angular_resource'
        },
        'angular_route': {
            deps: ['angular'],
            exports: 'angular_route'
        },
        'angular_timer': {
            deps: ['angular','moment'],
            exports: 'angular_timer'
        },
        'angular_ui_router': {
            deps: ['angular'],
            exports: 'angular_ui_router'
        },
        'fullcalendar': {
            deps: ['moment']
        }
    },

    deps: ['boot']
});