define([
    'angular',
    './angular-modules',

    './shared/header/index',
    './shared/api/index',
    './shared/auth/index',
    './shared/dialogs/index',
    './shared/search/index',

    './components/home/index',
    './components/login/index',
    './components/cards/index',
    './components/profile/index'
], function (ng) {
    'use strict';

    var TokenInterceptor = ['$httpProvider', function TokenInterceptor($httpProvider) {
        $httpProvider.interceptors.push('TokenInterceptor');
    }];

    var LoggedIn = ['$rootScope', '$state', 'AuthenticationService', function($rootScope, $state, AuthenticationService) {
        $rootScope.$on('$stateChangeSuccess', function(event, nextRoute, currentRoute) {

            if (nextRoute && nextRoute.access) {
                if (nextRoute.access.requiredLogin && !AuthenticationService.isLogged()) {
                    $state.go('root.login');
                }
            }
        });
    }];

    return ng.module('app', [

        'shared/api',
        'shared/auth',
        'shared/header',
        'shared/dialogs',
        'shared/search',
        
        'login',
        'home',
        'cards',
        'profile',

        'ngMaterial',
        'ngMessages',
        'ui.router',
        'timer'
    ]).config(TokenInterceptor).run(LoggedIn);
});