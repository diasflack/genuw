define(['./app'], function (app) {
    'use strict';

    return app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/home');
        $urlRouterProvider.when('/','/home');

        $stateProvider
            .state('root', {
                url:'/',
                templateUrl: 'app/shared/layout/layoutView.html',
                access: { requiredLogin: true }
            })
            .state('root.home', {
                url:'home',
                views: {
                    'content': {
                        templateUrl: 'app/components/home/homeView.html'
                    }
                },
                access: { requiredLogin: true }
            })
            .state('root.cardslist', {
                url:'cards',
                views: {
                    'content': {
                        templateUrl: 'app/components/cards/list/cardListView.html'
                    }
                },
                access: { requiredLogin: true }
            })
            .state('root.cardsdetail', {
                url:'cards/:cardId',
                views: {
                    'content': {
                        templateUrl: 'app/components/cards/detail/cardDetailView.html'
                    },
                    'detailsContent@root.cardsdetail' : {
                        templateUrl: 'app/components/cards/detail/segment/segmentListView.html'
                    }
                },
                access: { requiredLogin: true }
            })
            .state('root.cardsdetail.segment', {
                url:'/:segmentId',
                resolve: {
                    segmentPromise: ['$stateParams', 'ApiFactory', function($stateParams, ApiFactory){
                        return ApiFactory.segments.get({id:$stateParams.segmentId}).$promise;
                    }]
                },
                views: {
                    'detailsContent@root.cardsdetail': {
                        templateUrl: 'app/components/cards/detail/segment/segmentSingleView.html',
                        controller: 'SegmentSingleCtrl as segmentStudy'
                    }
                },
                access: { requiredLogin: true }
            })
            .state('root.profile', {
                url:'profile',
                views: {
                    'content': {
                        templateUrl: 'app/components/profile/profileView.html',
                        controller: 'ProfileCtrl'
                    }
                },
                access: { requiredLogin: true }
            })
            .state('root.login', {
                url:'login',
                views: {
                    'content': {
                        templateUrl: 'app/components/login/loginView.html'
                    }
                },
                access: { requiredLogin: false }
            })
            .state('root.logout', {
                url:'logout',
                views: {
                    'content': {
                        templateUrl: 'app/components/login/loginView.html',
                        controller: 'LogoutCtrl'
                    }
                },
                access: { requiredLogin: false }
            })

    }]);
});