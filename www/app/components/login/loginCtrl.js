define(['./module'], function (module) {
    'use strict';

    module.controller('LoginCtrl', ['$rootScope', '$state', '$window', 'UserService', 'ApiFactory', LoginCtrl]);

    function LoginCtrl($rootScope, $state, $window, UserService, Api) {
        var $this = this;

        $this.logIn = function(email, password) {
            if (email !== undefined && password !== undefined) {

                $this.loginError = '';

                UserService.logIn(email, password)
                    .then(function (res) {
                        $window.sessionStorage.token = res.data.access_token;
                        $window.sessionStorage.user = res.data.user._id;

                        $state.go('root.home');
                        $rootScope.$broadcast('loginStateChange');
                        },function (error) {
                            if (error.data.error === 'invalid_grant') {
                                $this.loginError = 'Invalid login/password combination';
                            } else {
                                $this.loginError = error.data.error_description;
                            }
                        });

            }
        };

        $this.register = function(email, password) {
            if (email !== undefined && password !== undefined) {

                var new_user = {email: email, password: password};

                Api.users.add(new_user).$promise.then(
                    function(){
                        $this.logIn(email, password);
                    },
                    function( error ){
                        $this.loginError = error.data.error;
                    });
            }
        }

    }
});