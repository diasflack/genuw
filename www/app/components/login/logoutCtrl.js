define(['./module'], function (module) {
    'use strict';

    module.controller('LogoutCtrl', ['$rootScope', '$scope', '$state', '$window', 'AuthenticationService', LogoutCtrl]);

    function LogoutCtrl($rootScope, $scope, $state, $window, AuthenticationService) {

        $scope.logout = function() {
            if (AuthenticationService.isLogged) {
                delete $window.sessionStorage.token;
                delete $window.sessionStorage.user;
                $state.go('root.login');

                $rootScope.$broadcast('loginStateChange');
            }
        };

        $scope.logout();
    }
});