define(['./../../module', '_'], function (module, _) {
    'use strict';

    module.controller('SegmentSingleCtrl', ['$scope', '$state', '$mdDialog', 'ApiFactory', 'timerLogic', 'segmentPromise', 'dialogTimerFinishedService', SegmentSingleCtrl]);

    function SegmentSingleCtrl($scope, $state, $mdDialog, ApiFactory, timerLogic, segmentPromise, dialogTimerFinishedService) {
        var $this = this;

        $this.segment = segmentPromise.segment;

        $this.id = $this.segment._id;

        $this.countdownVal = moment.duration($this.segment.timer).asSeconds();
        $this.timerPlay = true;
        timerLogic.init();

        $this.nextRepeat = function(lastRepeat, repeatInterval, repeated) {
            if (!repeatInterval.intervals[repeated]) return lastRepeat;
            return lastRepeat.getTime() + repeatInterval.intervals[repeated]*1000;
        };

        $this.rememberLevel = function(segment) {
            var level = 0,
                evaluatedTests = 0,
                tests = segment.tests;
            $.each(tests, function() {
                if (this.evaluation) {
                    level += parseInt(this.evaluation);
                    evaluatedTests++;
                }
            });

            return level/evaluatedTests;
        };

        $this.timerControl = function() {
            timerLogic.startLearning();
            $this.timerPlay = !$this.timerPlay;
        };

        $this.timerFinished = function() {
            dialogTimerFinishedService();
        };

        $this.addTest = function() {
            $this.segment.tests.push({question: '', answer: '', firstTime: true});
        };

        $this.deleteTest = function(i) {
            $this.segment.tests.splice(i, 1);
        };

        $this.showAnswer = function(index) {
            $this.segment.tests[index].showedAnswer = true;
        };

        $this.completeSegment = function(i) {

            $this.segment.repeated += 1;
            $this.segment.lastRepeat = new Date();
            $this.segment.nextRepeat = $this.nextRepeat($this.segment.lastRepeat, $this.segment.interval, $this.segment.repeated);
            $this.segment.rememberLevel = $this.rememberLevel($this.segment);

            ApiFactory.segments.save({id: $this.segment._id}, $this.segment)
                .$promise.then(function(){
                    ApiFactory.cards.get({id: $this.segment.card_id}, function(data){
                        $scope.$parent.detail.card = data.card;
                        $state.go('root.cardsdetail', {cardId: $this.segment.card_id})
                    });
                });
        };

    }
});