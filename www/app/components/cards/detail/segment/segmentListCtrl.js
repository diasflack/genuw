define(['./../../module'], function (module) {
    'use strict';

    module.controller('SegmentListCtrl', ['$scope', '$state', 'ApiFactory', 'dialogSegmentCreateService', 'dialogCardDeleteService', SegmentListCtrl]);

    function SegmentListCtrl($scope, $state, ApiFactory, dialogSegmentCreateService, dialogCardDeleteService) {
        var $this = this;

        $this.card_id = $state.params.cardId;

        $this.loadIntervals = function() {
            ApiFactory.intervals.getAll(function(data){
                $this.intervals = data;
            });
        };

        $this.segmentAdd = function($event) {
            dialogSegmentCreateService($event, $this.card_id)
                .then(function(response){
                    $scope.$parent.detail.getCard($this.card_id);
                });
        };

        $this.segmentSave = function(segment) {
            $this.status = [];

            ApiFactory.segments.save({id: segment._id}, segment).$promise.then(
                function(value) {
                    $this.status[segment._id] = 'ok';
                },
                function(error) {
                    $this.status[segment._id] = 'err';
                }
            );

        };

        $this.confirmDelete = function(event, segment) {
            dialogCardDeleteService(event, 'segments', segment._id)
                .then(function(){
                    ApiFactory.cards.get({id: segment.card_id}, function(data){
                        $scope.$parent.detail.card = data.card;
                    });
                }
            );
        };

    }
});