define(['./../module'], function (module) {
    'use strict';

    module.controller('CardDetailCtrl', ['$state', 'ApiFactory', 'AuthenticationService', CardDetailCtrl]);

    function CardDetailCtrl($state, ApiFactory, AuthenticationService) {

        var $this = this;

        $this.id = $state.params.cardId;

        $this.getCard = function(cardId) {
            ApiFactory.cards.get({id: cardId}, function(data){ $this.card = data.card;});
        };

        $this.getCard($this.id);

        var userID = AuthenticationService.user();

    }
});