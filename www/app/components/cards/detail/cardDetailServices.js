define(['./../module'], function (module) {
    'use strict';

    module.factory('timerLogic', TimerLogic);

    function TimerLogic() {
        var timer,timerStarted,countdownVal,learnBtn;

        function init() {
            timer = document.getElementById('timer');
            timerStarted = true;
            countdownVal = 0;
        }

        function getLearnBtn() {
            return learnBtn;
        }

        function getStartedDate() {
            return new Date()
        }

        function setCountdownVal(scope, newCountdownVal) {
            scope.$broadcast('timer-set-countdown', newCountdownVal);
            scope.$broadcast('timer-reset');
            scope.$broadcast('timer-clear');
        }

        function startLearning() {

            if (!timerStarted) {
                timer.start();
                timerStarted = true;
            } else if (timerStarted === true) {
                timer.stop();
                timerStarted = 'resume';
            } else {
                timer.resume();
                timerStarted = true;
            }

            return true;
        }

        function endLearning(scope) {
            timerStarted = false;
            learnBtn = 'Начать изучение нового сегмента';
            scope.$broadcast('timer-set-countdown', countdownVal);
            timer.start();
            timer.stop();

            return false;
        }


        return {
            init: init,
            startLearning: startLearning,
            endLearning: endLearning,
            getLearnBtn: getLearnBtn,
            getStartedDate: getStartedDate,
            setCountdownVal: setCountdownVal
        };
    }

});