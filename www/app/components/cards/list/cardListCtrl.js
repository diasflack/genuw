define(['./../module'], function (module) {
    'use strict';

    module.controller('CardListCtrl', ['ApiFactory', 'AuthenticationService', 'dialogCardCreateService', 'dialogCardDeleteService', CardListCtrl]);

    function CardListCtrl(ApiFactory, AuthenticationService, dialogCardCreateService, dialogCardDeleteService) {
        var $this = this;

        var userID = AuthenticationService.user();

        ApiFactory.cards.getAll(function(data){
            $this.cards = data;
        });

        $this.loadTypes = function() {
            ApiFactory.types.getAll(function(data){
                $this.types = data;
            });
        };

        $this.cardAdd = function($event) {
            dialogCardCreateService($event);
        };

        $this.onTypeSelect = function(index) {
            if ($this.cards[index].type.additionalFields.length === 0) {
                $this.cards[index].additionalFields = [];
            }
        };


        $this.cardSave = function(index) {
            $this.status = [];
            ApiFactory.cards.save({id: this.cards[index]._id}, this.cards[index]).$promise.then(
                function(value) {
                    $this.status[index] = 'ok';
                },
                function(error) {
                    $this.status[index] = 'err';
                }
            );

        };

        $this.confirmDelete = function(event, card) {
            dialogCardDeleteService(event, 'cards', card._id)
                .then(function(){
                    ApiFactory.cards.getAll(function(data){$this.cards = data});
                }
            );
        };

    }
});