define(['./module'], function (module) {
    'use strict';

    module.controller('ProfileCtrl', ['ApiFactory', 'AuthenticationService', ProfileCtrl]);

    function ProfileCtrl(Api, AuthenticationService) {
        var $this = this;

        var userID = AuthenticationService.user();

        Api.users.get({id: userID}, function(data){
            $this.user = data.user;
        });

        $this.saveUser = function() {
            Api.users.save({id:userID}, $this.user)
                .$promise.then(
                //success
                function( value ){
                    $this.profileSuccess = 'User successfully changed!';
                    $this.profileError ? delete $this.profileError : '';
                },
                //error
                function( error ){
                    $this.profileError = error.data.error;
                    $this.profileSuccess ? delete $this.profileSuccess : '';
                }
            )
        }

    }

});