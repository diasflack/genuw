define(['./module','fullcalendar'], function (module) {
    'use strict';

    module.controller('HomeCtrl', ['ApiFactory', 'AuthenticationService', 'dialogCardCreateService', HomeCtrl]);

    function HomeCtrl(Api, AuthenticationService, dialogCardCreateService) {
        var $this = this;
        var userID = AuthenticationService.user();

        function nextRepeat(lastRepeat, repeatInterval, repeated) {
            lastRepeat = new Date(lastRepeat);
            if (!repeatInterval[repeated]) return lastRepeat;
            return lastRepeat.getTime() + repeatInterval[repeated]*1000;
        }

        $this.cardAdd = function($event) {
            dialogCardCreateService($event);
        };

        Api.segments.getAll(function(data){
            $this.segments = data;

            var events = [];

            var events_colors = [
                {
                    backgroundColor: '#389090',
                    borderColor: '#389090'
                },
                {
                    backgroundColor: '#F47E7D',
                    borderColor: '#F47E7D'
                },
                {
                    backgroundColor: '#B5D045',
                    borderColor: '#B5D045'
                },
                {
                    backgroundColor: '#FB8335',
                    borderColor: '#FB8335'
                },
                {
                    backgroundColor: '#81C0C5',
                    borderColor: '#81C0C5'
                }
            ];

            $.each($this.segments, function(){
                var event = {};

                event.title = this.title;
                event.url = '#/cards/'+this.card_id+'/'+this._id;
                event.allDay = false;
                event.backgroundColor = '#000';
                event.borderColor = '#000';

                var intervals = this.interval.intervals;
                var eventsNumber = intervals.length;

                for (var i=0;i < eventsNumber;i++){

                    var start = nextRepeat(this.started,intervals,i);

                    var new_event = Object.create(event);
                    new_event.start = start;

                    if (start > new Date().getTime()) {
                        new_event.backgroundColor = events_colors[i].backgroundColor;
                        new_event.borderColor = events_colors[i].borderColor;
                    }

                    events.push(new_event);
                }

            });

            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,basicWeek,basicDay'
                },
                timezone: 'local',
                events: events
            });
        });


    }
});