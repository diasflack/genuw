define(['./module', './searchCtrl'], function(module, SearchCtrl) {
    'use strict';

    module.directive('search', search);

    function search() {
        return {
            restrict: 'E',
            replace: false,
            scope: {},
            templateUrl: 'app/shared/search/searchView.html',
            controller: SearchCtrl,
            controllerAs: 'searchCtrl'
        };
    }

});