define([], function(){
    'use strict';

    return ['$state', '$mdSidenav', 'ApiFactory', function ($state, $mdSidenav, ApiFactory) {
        var $this = this;

        $this.tagsList = function(query) {
            return ApiFactory.search.get({tag: query}).$promise;
        };

        $this.searchTextChange = function(text) {

        };

        $this.selectedItemChange = function(item) {
            if (item) {
                $state.go('root.cardsdetail', {cardId: item._id});
                $mdSidenav('left').isOpen() ? $mdSidenav('left').close() : '';
            }
        }

    }];
});
