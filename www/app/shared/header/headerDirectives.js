define(['./module', './headerCtrl'], function(module, HeaderCtrl) {
    'use strict';

    module.directive('navMain', navMain)
        .directive('activeLink', activeLink);

    function navMain() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/shared/header/headerView.html',
            controller: HeaderCtrl
        };
    }

    function activeLink() {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {

                var clazz = attrs.activeLink;
                var path = attrs.ngHref;
                var newPath = location.hash;

                if (path === newPath) {
                    element.addClass(clazz);
                } else {
                    element.removeClass(clazz);
                }
            }

        };
    }

});