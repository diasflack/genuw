define([], function(){
    'use strict';

    return ['$scope', '$mdSidenav', '$mdUtil', 'AuthenticationService', function ($scope, $mdSidenav, $mdUtil, AuthenticationService) {

        $scope.toggleSidenav = buildToggler('left');

        $scope.isLogged = AuthenticationService.isLogged();

        $scope.$on('loginStateChange', function() {
            $scope.isLogged = AuthenticationService.isLogged();
        });

        function buildToggler(navID) {
            var debounceFn =  $mdUtil.debounce(function(){
                $mdSidenav(navID).toggle()
            },300);
            return debounceFn;
        }

    }];

});
