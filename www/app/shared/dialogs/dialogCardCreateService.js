define(['./module','./dialogCardCreateCtrl'], function(module, ctrl) {
    'use strict';

    module.factory('dialogCardCreateService', ['$mdDialog', dialogCardCreateService]);

    function dialogCardCreateService($mdDialog){
        return function(event) {
                $mdDialog.show({
                    controller: ctrl,
                    templateUrl: 'app/shared/dialogs/dialogCardCreateView.html',
                    targetEvent: event
                })
            }
        }

});