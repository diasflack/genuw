define([], function(){

    var DialogController = ['$scope', '$state', '$mdDialog', 'ApiFactory', 'CardModel', 'AuthenticationService', function ($scope, $state, $mdDialog, ApiFactory, CardModel, AuthenticationService) {

        $scope.newCard = new CardModel();
        $scope.newCard.user_id = AuthenticationService.user();

        $scope.loadTypes = function() {
            ApiFactory.types.getAll(function(data){
                $scope.types = data;
            });
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.cardCreate = function() {
            ApiFactory.cards.add($scope.newCard, function(data){
                $state.go('root.cardsdetail',{cardId:data.card._id});
                $mdDialog.hide();
            });

        }

    }];

    return DialogController;
});