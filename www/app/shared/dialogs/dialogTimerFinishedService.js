define(['./module'], function(module) {
    'use strict';

    module.factory('dialogTimerFinishedService', ['$mdDialog', dialogTimerFinishedService]);

    function dialogTimerFinishedService($mdDialog){
        return function() {
                return $mdDialog.show({
                    controller: function() {
                        var $this = this;
                        $this.cancel = function() {
                            $mdDialog.cancel();
                        };
                    },
                    controllerAs: 'dlgTimer',
                    templateUrl: 'app/shared/dialogs/dialogTimerFinishedView.html'
                })
            }
        }

});