define([], function(){

    var DialogController = ['$mdDialog', '$state', 'ApiFactory', 'SegmentModel', 'AuthenticationService', 'card_id', function ($mdDialog, $state, ApiFactory, SegmentModel, AuthenticationService, card_id) {
        var $this = this;

        $this.newSegment = new SegmentModel();
        $this.newSegment.card_id = card_id;
        $this.newSegment.user_id = AuthenticationService.user();
        $this.newSegment.timer = '00:25';

        $this.loadIntervals = function() {
            ApiFactory.intervals.getAll(function(data){
                $this.intervals = data;
            });
        };

        $this.cancel = function() {
            $mdDialog.cancel();
        };

        $this.segmentCreate = function() {
            ApiFactory.cards.add({id: card_id, segment: "segment"}, $this.newSegment, function(data){
                $state.go("root.cardsdetail.segment",{segmentId:data.segment._id});
                $mdDialog.hide();
            });
        }

    }];

    return DialogController;
});