define(['./module','./dialogSegmentCreateCtrl'], function(module, ctrl) {
    'use strict';

    module.factory('dialogSegmentCreateService', ['$mdDialog', dialogSegmentCreateService]);

    function dialogSegmentCreateService($mdDialog){
        return function(event, card_id) {
                return $mdDialog.show({
                    controller: ctrl,
                    controllerAs: 'dialog',
                    locals: { card_id: card_id },
                    templateUrl: 'app/shared/dialogs/dialogSegmentCreateView.html',
                    targetEvent: event
                })
            }
        }

});