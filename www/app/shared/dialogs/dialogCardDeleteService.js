define(['./module','./dialogCardCreateCtrl'], function(module, ctrl) {
    'use strict';

    module.factory('dialogCardDeleteService', ['$mdDialog', 'ApiFactory', dialogCardDeleteService]);

    function dialogCardDeleteService($mdDialog, ApiFactory){
        return function(event, objectType, objectId) {

            var confirm = $mdDialog.confirm()
                .title('Would you like to delete this study card?')
                .ok('Yes, delete!')
                .cancel('Nope')
                .targetEvent(event);

            return $mdDialog.show(confirm)
                .then(function() {
                   return ApiFactory[objectType].remove({id: objectId}).$promise;
                });
            }
        }

});