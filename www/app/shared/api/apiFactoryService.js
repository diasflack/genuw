define(['./module'], function(module) {
    'use strict';

    module.factory('ApiFactory', ['$resource', function($resource) {
        return {
            cards: $resource('/api/v1/cards/:id/:segment/', {id: '@id', segment: '@segment'}, {
                get: {method: 'GET', params: {}, isArray: false},
                add: {method: 'POST', data: {}, isArray: false},
                save: {method: 'PUT', data: {}, isArray: false},
                getAll: {method: 'GET', data: {}, isArray: true},
                remove: {method: 'DELETE' }
            }),
            search: $resource('/api/v1/search', {}, {
                get: {method: 'GET', data: {}, isArray: true}
            }),
            types: $resource('/api/v1/types/:id/', {id: '@id'}, {
                get: {method: 'GET', params: {}, isArray: false},
                add: {method: 'POST', data: {}, isArray: false},
                save: {method: 'PUT', data: {}, isArray: false},
                getAll: {method: 'GET', data: {}, isArray: true},
                remove: {method: 'DELETE' }
            }),
            intervals: $resource('/api/v1/intervals/:id/', {id: '@id'}, {
                get: {method: 'GET', params: {}, isArray: false},
                add: {method: 'POST', data: {}, isArray: false},
                save: {method: 'PUT', data: {}, isArray: false},
                getAll: {method: 'GET', data: {}, isArray: true},
                remove: {method: 'DELETE' }
            }),
            segments: $resource('/api/v1/segments/:id', {id: '@id'}, {
                get: {method: 'GET', params: {}, isArray: false},
                add: {method: 'POST', data: {}, isArray: false},
                save: {method: 'PUT', data: {}, isArray: false},
                getAll: {method: 'GET', data: {}, isArray: true},
                remove: {method: 'DELETE' }
            }),
            users: $resource('/api/v1/users/:id', {id: '@id'}, {
                get: {method: 'GET', params: {}, isArray: false},
                add: {method: 'POST', data: {}, isArray: false},
                save: {method: 'PUT', data: {}, isArray: false},
                getAll: {method: 'GET', data: {}, isArray: true},
                remove: {method: 'DELETE' }
            }),
            clients: $resource('/api/v1/clients/:id', {id: '@id'}, {
                get: {method: 'GET', params: {}, isArray: false},
                add: {method: 'POST', data: {}, isArray: false},
                save: {method: 'PUT', data: {}, isArray: false},
                getAll: {method: 'GET', data: {}, isArray: true},
                remove: {method: 'DELETE' }
            })
        };
    }]);

});