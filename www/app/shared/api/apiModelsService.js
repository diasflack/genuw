define(['./module'], function(module) {
    'use strict';

    module.factory("SegmentModel", SegmentModel)
        .factory("CardModel", CardModel)
        .factory("TypeModel", TypeModel)
        .factory("IntervalModel", IntervalModel);

    function SegmentModel() {
        function Segment() {
            this.user_id = 0;
            this.card_id = 0;

            this.title = '';
            this.started = new Date();

            this.timer = '00:25';
            this.repeated = 0;
            this.interval = '';
            this.lastRepeat = new Date();
            this.nextRepeat = new Date();

            this.stopPoint = '';
            this.tests = [];
            this.rememberLevel = '';
        }

        return Segment
    }

    function CardModel() {
        function Card() {
            this.user_id = 0;
            this.title = '';
            this.type = '';
            this.tags = [];
            this.segments = [];
            this.started = new Date();
            this.rememberLevel = ' ';
            this.additionalFields = [];
        }

        return Card
    }

    function TypeModel() {
        function Type() {
            this.user_id = 0;
            this.title = ' ';
            this.card_ids = [];
        }

        return Type
    }

    function IntervalModel() {
        function Interval() {
            this.user_id = 0;
            this.title = ' ';
            this.intervals = [];
        }

        return Interval
    }
});