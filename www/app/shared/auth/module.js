define(['angular'], function(ng) {
    'use strict';
    return ng.module('shared/auth', []);
    }
);