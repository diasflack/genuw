define(['./module'], function(module) {
    'use strict';

    module.factory('UserService', ['$http', function ($http) {
        return {
            logIn: function(email, password) {
                var loginInfo = {
                    grant_type: 'password',
                    client_id: 'webV1',
                    client_secret: '123456',
                    username: email,
                    password: password
                };

                return $http.post('/oauth/token', loginInfo);
            },

            logOut: function() {

            }
        }
    }]);

});