define(['./module'], function(module) {
    'use strict';

    module.factory('AuthenticationService', ['$window', function AuthenticationService($window) {
        return {
            isLogged: function () {
                return $window.sessionStorage.token ? true : false
            },
            user: function () {
                return $window.sessionStorage.user
            }
        };
    }]);

});