var mongoose    = require("mongoose");
var log         = require("./log")(module);
var config      = require("./config");
var crypto      = require("crypto");

//DB INIT
mongoose.connect(config.get("mongoose:uri"));
var db = mongoose.connection;

db.on("error", function (err) {
    log.error("connection error:", err.message);
});
db.once("open", function callback () {
    log.info("Connected to DB!");
});

//MODEL REQUIRE
var CardModel = require("./../api/models/card").CardModel;
var IntervalModel = require("./../api/models/interval").IntervalModel;
var SegmentModel = require("./../api/models/segment").SegmentModel;
var TypeModel = require("./../api/models/type").TypeModel;
var UserModel = require("./../api/models/user");

//MODEL EXPORT

module.exports.CardModel = CardModel;
module.exports.IntervalModel = IntervalModel;
module.exports.SegmentModel = SegmentModel;
module.exports.TypeModel = TypeModel;

module.exports.UserModel = UserModel.UserModel;
module.exports.ClientModel = UserModel.ClientModel;
module.exports.AccessTokenModel = UserModel.AccessTokenModel;
module.exports.RefreshTokenModel = UserModel.RefreshTokenModel;



