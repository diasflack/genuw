var express = require('express');
var path = require('path');
var passport = require('passport');
var log = require('./libs/log')(module);
var config = require('./libs/config');
var oauth2 = require('./libs/oauth2');
var app = express();

var routes = require('./api/routes');

app.use(express.favicon()); // отдаем стандартную фавиконку, можем здесь же свою задать
app.use(express.logger('dev')); // выводим все запросы со статусами в консоль
app.use(express.bodyParser()); // стандартный модуль, для парсинга JSON в запросах
app.use(express.methodOverride()); // поддержка put и delete
app.use(passport.initialize());
app.use(express.static(path.join(__dirname, "www"))); // запуск статического файлового сервера, который смотрит на папку public/ (в нашем случае отдает index.html)
app.use(app.router); // модуль для простого задания обработчиков путей

app.get('/api', function (req, res) {
    res.send('API is running');
});


var port = Number(process.env.PORT || 5000);
app.listen(port, function(){
    log.info('Express server listening on port ' + port);
});

/* Error handling */
app.use(function(req, res, next){
    res.status(404);
    log.debug('Not found URL: %s',req.url);
    res.send({ error: 'Not found' });
    return;
});

app.use(function(err, req, res, next){
    res.status(err.status || 500);
    log.error('Internal error(%d): %s',res.statusCode,err.message);
    res.send({ error: err.message });
    return;
});

app.get('/ErrorExample', function(req, res, next){
    next(new Error('Random error!'));
});

/* Authorization */

require('./libs/passport');

app.post('/oauth/token', oauth2.token);

/*
 * RESTFUL API
 */

app.get('/api/v1/cards', passport.authenticate('bearer', { session: false }), routes.cards.readAll);
app.post('/api/v1/cards', passport.authenticate('bearer', { session: false }), routes.cards.create);
app.get('/api/v1/cards/:id', passport.authenticate('bearer', { session: false }), routes.cards.read);
app.put('/api/v1/cards/:id', passport.authenticate('bearer', { session: false }), routes.cards.update);
app.delete('/api/v1/cards/:id', passport.authenticate('bearer', { session: false }), routes.cards.delete);

app.get('/api/v1/search', passport.authenticate('bearer', { session: false }), routes.cards.searchByTag);
app.post('/api/v1/cards/:id/segment', passport.authenticate('bearer', { session: false }), routes.cards.createSegment);

app.get('/api/v1/segments', passport.authenticate('bearer', { session: false }), routes.segments.readAll);
app.post('/api/v1/segments', passport.authenticate('bearer', { session: false }), routes.segments.create);
app.get('/api/v1/segments/:id', passport.authenticate('bearer', { session: false }), routes.segments.read);
app.put('/api/v1/segments/:id', passport.authenticate('bearer', { session: false }), routes.segments.update);
app.delete('/api/v1/segments/:id', passport.authenticate('bearer', { session: false }), routes.segments.delete);

app.get('/api/v1/types', passport.authenticate('bearer', { session: false }), routes.types.readAll);
app.post('/api/v1/types', passport.authenticate('bearer', { session: false }), routes.types.create);
app.get('/api/v1/types/:id', passport.authenticate('bearer', { session: false }), routes.types.read);
app.put('/api/v1/types/:id', passport.authenticate('bearer', { session: false }), routes.types.update);
app.delete('/api/v1/types/:id', passport.authenticate('bearer', { session: false }), routes.types.delete);

app.get('/api/v1/intervals', passport.authenticate('bearer', { session: false }), routes.intervals.readAll);
app.post('/api/v1/intervals', passport.authenticate('bearer', { session: false }), routes.intervals.create);
app.get('/api/v1/intervals/:id', passport.authenticate('bearer', { session: false }), routes.intervals.read);
app.put('/api/v1/intervals/:id', passport.authenticate('bearer', { session: false }), routes.intervals.update);
app.delete('/api/v1/intervals/:id', passport.authenticate('bearer', { session: false }), routes.intervals.delete);

app.get('/api/v1/users', passport.authenticate('bearer', { session: false }), routes.users.readAll);
app.post('/api/v1/users', routes.users.create);
app.get('/api/v1/users/:id', passport.authenticate('bearer', { session: false }), routes.users.read);
app.put('/api/v1/users/:id', passport.authenticate('bearer', { session: false }), routes.users.update);
app.delete('/api/v1/users/:id', passport.authenticate('bearer', { session: false }), routes.users.delete);

app.get('/api/v1/clients', passport.authenticate('bearer', { session: false }), routes.clients.readAll);
app.post('/api/v1/clients', passport.authenticate('bearer', { session: false }), routes.clients.create);
app.get('/api/v1/clients/:id', passport.authenticate('bearer', { session: false }), routes.clients.read);
app.put('/api/v1/clients/:id', passport.authenticate('bearer', { session: false }), routes.clients.update);
app.delete('/api/v1/clients/:id', passport.authenticate('bearer', { session: false }), routes.clients.delete);
