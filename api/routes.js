var routes = {};
routes.cards = require('./routes/cards');
routes.segments = require('./routes/segments');
routes.types = require('./routes/types');
routes.intervals = require('./routes/intervals');
routes.users = require('./routes/users');
routes.clients = require('./routes/clients');

exports.cards = routes.cards;
exports.segments = routes.segments;
exports.intervals = routes.intervals;
exports.types = routes.types;
exports.users = routes.users;
exports.clients = routes.clients;