var CardModel = require('./../../libs/mongoose').CardModel;
var TypeModel = require('./../../libs/mongoose').TypeModel;
var SegmentModel = require('./../../libs/mongoose').SegmentModel;
var log = require('./../../libs/log')(module);
var _u = require("underscore");



var extend = require('node.extend');

exports.readAll = function(req, res) {
    return CardModel.find({user_id:req.user._id})
        .deepPopulate('type')
        .exec(function (err, cards) {
            if (!err) {
                log.info('Cards send');
                return res.send(cards);
            } else {
                res.statusCode = 500;
                log.error('Internal error(%d): %s',res.statusCode,err.message);
                return res.send({ error: 'Server error' });
            }
        });
};

exports.searchByTag = function(req, res) {

    if(!req.query) {
        res.statusCode = 404;
        return res.send({ error: 'No query found' });
    }

    var tag_reg = new RegExp(req.query.tag, 'gi');

    return CardModel.find({user_id:req.user._id, tags: {$regex: tag_reg} })
        .deepPopulate('type')
        .exec(function (err, cards) {
            if (!err) {
                return res.send(cards);
            } else {
                res.statusCode = 500;
                log.error('Internal error(%d): %s',res.statusCode,err.message);
                return res.send({ error: 'Server error' });
            }
        });
};

exports.create = function(req, res) {

    var card = new CardModel();

    req.body.type = req.body.type._id;

    card = extend(true, card, req.body);

    card.save(function (err) {
        if (!err) {
            log.info("card created");
            return TypeModel.findById(card.type, function(err,type){
                if (!err) {
                    type.card_ids.push(card._id);

                    type.save(function(err){
                        if (!err) {
                            log.info("type updated");
                            return res.send({ status: 'OK', card:card });
                        }
                    })
                } else {
                    res.statusCode = 500;
                    res.send({ error: 'Server error' });
                    log.error('Internal error(%d): %s',res.statusCode,err.message);
                }
            });

        } else {
            if(err.name == 'ValidationError') {
                res.statusCode = 400;
                res.send({ error: 'Validation error' });
            } else {
                res.statusCode = 500;
                res.send({ error: 'Server error' });
            }
            log.error('Internal error(%d): %s',res.statusCode,err.message);
        }
    });
};

exports.createSegment = function(req, res) {

    if(!req.body) {
        res.statusCode = 404;
        return res.send({ error: 'New segment not found' });
    }

    req.body.interval = req.body.interval._id;

    var segment = new SegmentModel();
    segment = extend(true, segment, req.body);
    
    segment.save(function (err) {
        if (!err) {
            log.info("Segment created");
        } else {
            console.log(err);
            log.error('Internal error(%d): %s',res.statusCode,err.message);
        }
    });


    return CardModel.findById(req.params.id, function (err, card) {
        if(!card) {
            res.statusCode = 404;
            return res.send({ error: 'Card Not found' });
        }

        card.segments.push(segment._id);
        
        return card.save(function (err) {
            if (!err) {
                log.info("card updated - segment created");
                return CardModel.findOne(card).populate('segments').exec(function (err, card) {
                    return res.send({status: 'OK', card: card, segment:segment})
                });
            } else {
                if(err.name == 'ValidationError') {
                    res.statusCode = 400;
                    res.send({ error: 'Validation error' });
                } else {
                    res.statusCode = 500;
                    res.send({ error: 'Server error' });
                }
                log.error('Internal error(%d): %s',res.statusCode,err.message);
            }
        });
    });
};

exports.read = function(req, res) {
    return CardModel.findById(req.params.id)
        .deepPopulate('type segments segments.interval')
        .exec(function (err, card) {
            if(!card) {
                res.statusCode = 404;
                return res.send({ error: 'Not found' });
            }
            if (!err) {
                return res.send({ status: 'OK', card:card });
            } else {
                res.statusCode = 500;
                log.error('Internal error(%d): %s',res.statusCode,err.message);
                return res.send({ error: 'Server error' });
            }
        });
};

exports.update = function (req, res){
    return CardModel.findById(req.params.id, function (err, card) {
        if(!card) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }

        req.body.title ? card.title = req.body.title : '';
        req.body.additionalFields ? card.additionalFields = req.body.additionalFields : '';
        req.body.tags ? card.tags = req.body.tags : '';

        if (req.body.type._id !== card.type) {

            card.pre('save', function(next){
                this.model('Type').update(
                    {_id: this.type},
                    {$pull: {card_ids: this._id}},
                    next
                );
                this.model('Type').update(
                    {_id: req.body.type._id},
                    {$push: {card_ids: this._id}},
                    next
                );
            });

            card.type = req.body.type._id;

        }

        return card.save(function (err) {
            if (!err) {
                log.info("card updated");
                return res.send({ status: 'OK', card:card });
            } else {
                if(err.name == 'ValidationError') {
                    res.statusCode = 400;
                    res.send({ error: 'Validation error' });
                } else {
                    res.statusCode = 500;
                    res.send({ error: 'Server error' });
                }
                log.error('Internal error(%d): %s',res.statusCode,err.message);
            }
        });
    });
};

exports.delete = function (req, res){
    return CardModel.findById(req.params.id, function (err, card) {
        if(!card) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        
        card.pre('remove', function(next){
		    this.model('Segment').remove(
		        {card_id: this._id}, 
		        next
		        );
            this.model('Type').update(
                {_id: this.type},
                {$pull: {card_ids: this._id}},
                next
            );
		});
        
        return card.remove(function (err) {
            if (!err) {
                log.info("card removed");
                return res.send({ status: 'OK' });
            } else {
                res.statusCode = 500;
                log.error('Internal error(%d): %s',res.statusCode,err.message);
                return res.send({ error: 'Server error' });
            }
        });
    }); 
};