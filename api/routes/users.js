var UserModel = require('./../../libs/mongoose').UserModel;
var IntervalModel = require('./../../libs/mongoose').IntervalModel;
var TypeModel = require('./../../libs/mongoose').TypeModel;
var log = require('./../../libs/log')(module);
var extend = require('node.extend');

exports.readAll = function(req, res) {
    return UserModel.find(function (err, users) {
        if (!err) {
            return res.send(users);
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
};

exports.create = function(req, res) {
    return UserModel.findOne({ email: req.body.email }, function(err, usr) {
        if (usr) {
            res.statusCode = 400;
            return res.send({ error: 'User already exist' });
        } else {
            var user = new UserModel({ email: req.body.email, password: req.body.password });
            user.save(function (err) {
                if (!err) {
                    log.info("user created");

                    var basicIntervals = [
                        {
                            title: "Fast [0 - 20m - 8h - 24h]",
                            intervals: [0,1200,28800,86400]
                        },
                        {
                            title: "Normal [0 - 20m - 24h - 48h - 72h]",
                            intervals: [0,1200,86400,172800,259200]
                        },
                        {
                            title: "Deep [0 - 20m - 48h - 3w - 3mth]",
                            intervals: [0,1200,172800,1814400,7776000]
                        }
                    ];

                    var basicTypes = [
                        {
                            title: "Web article",
                            additionalFields: ["Link"]
                        },
                        {
                            title: "Book",
                            additionalFields:[]
                        },
                        {
                            title: "Facts",
                            additionalFields:[]
                        }
                    ];

                    for (var i = 0, bl = basicIntervals.length; i < bl; i++) {

                        var interval = new IntervalModel();

                        interval.user_id = user._id;
                        interval.title = basicIntervals[i].title;
                        interval.intervals = basicIntervals[i].intervals;

                        interval.save(function (err) {
                            if (!err) {
                                log.info("interval created");
                            } else {
                                console.log(err);
                                if(err.name == 'ValidationError') {
                                    res.statusCode = 400;
                                    res.send({ error: 'Validation error' });
                                } else {
                                    res.statusCode = 500;
                                    res.send({ error: 'Server error' });
                                }
                                log.error('Internal error(%d): %s',res.statusCode,err.message);
                            }
                        });
                    }

                    for (var i = 0, tl = basicTypes.length; i < tl; i++) {

                        var type = new TypeModel();

                        type.user_id = user._id;
                        type.title = basicTypes[i].title;
                        type.additionalFields = basicTypes[i].additionalFields;

                        type.save(function (err) {
                            if (!err) {
                                log.info("type created");
                            } else {
                                console.log(err);
                                if(err.name == 'ValidationError') {
                                    res.statusCode = 400;
                                    res.send({ error: 'Validation error' });
                                } else {
                                    res.statusCode = 500;
                                    res.send({ error: 'Server error' });
                                }
                                log.error('Internal error(%d): %s',res.statusCode,err.message);
                            }
                        });
                    }

                    return res.send({ status: 'OK', user:user });
                } else {
                    console.log(err);
                    if(err.name == 'ValidationError') {
                        res.statusCode = 400;
                        res.send({ error: 'Validation error' });
                    } else {
                        res.statusCode = 500;
                        res.send({ error: 'Server error' });
                    }
                    log.error('Internal error(%d): %s',res.statusCode,err.message);
                }
            });

        }
    });

};

exports.read = function(req, res) {
    return UserModel.findById(req.params.id, function (err, user) {
        if(!user) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        if (!err) {
            var user_sent = {
                _id: user._id,
                email: user.email
            };
            return res.send({ status: 'OK', user:user_sent });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
}

exports.update = function(req, res){
    return UserModel.findById(req.params.id, function (err, user) {
        if(!user) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }

        if (!user.checkPassword(req.body.old_password)) {
            res.statusCode = 400;
            return res.send({ error: 'Old password incorrect' });
        }

        user.password = req.body.new_password;
        user.email = req.body.email;
        
        return user.save(function (err) {
            if (!err) {
                log.info("user updated");
                return res.send({ status: 'OK', user:user });
            } else {
                if(err.name == 'ValidationError') {
                    res.statusCode = 400;
                    res.send({ error: 'Validation error' });
                } else if(err.name == 'MongoError') {
                    res.statusCode = 400;
                    res.send({ error: 'User already exist' });
                } else {
                    res.statusCode = 500;
                    res.send({ error: 'Server error' });
                }
                log.error('Internal error(%d): %s',res.statusCode,err.message);
            }
        });
    });
};

exports.delete = function (req, res){
    return UserModel.findById(req.params.id, function (err, user) {
        if(!user) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        
        user.pre('remove', function(next){
		    this.model('Segment').remove({user_id: this._id}, next);
		    this.model('Card').remove({user_id: this._id}, next);
		    this.model('Type').remove({user_id: this._id}, next);
            this.model('Interval').remove({user_id: this._id}, next);
		});
		
        return user.remove(function (err) {
            if (!err) {
                log.info("user removed");
                return res.send({ status: 'OK' });
            } else {
                res.statusCode = 500;
                log.error('Internal error(%d): %s',res.statusCode,err.message);
                return res.send({ error: 'Server error' });
            }
        });
    }); 
};