var SegmentModel = require('./../../libs/mongoose').SegmentModel;
var log = require('./../../libs/log')(module);
var extend = require('node.extend');

exports.readAll = function(req, res) {
    return SegmentModel.find({user_id:req.user._id}, function (err, segments) {
        if (!err) {
            return res.send(segments);
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    }).populate('interval');
};

exports.create = function(req, res) {

    var segment = new SegmentModel();
    segment = extend(true, segment, req.body);

    segment.save(function (err) {
        if (!err) {
            log.info("segment created");
            return SegmentModel.findOne(segment).exec(function (err, segment) {
                return res.send({status: 'OK', segment: segment})
            });
        } else {
            console.log(err);
            if(err.name == 'ValidationError') {
                res.statusCode = 400;
                res.send({ error: 'Validation error' });
            } else {
                res.statusCode = 500;
                res.send({ error: 'Server error' });
            }
            log.error('Internal error(%d): %s',res.statusCode,err.message);
        }
    });
};

exports.read = function(req, res) {
    return SegmentModel.findById(req.params.id, function (err, segment) {
        if(!segment) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        if (!err) {
            return res.send({ status: 'OK', segment:segment });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    }).populate('interval');
};

exports.update = function (req, res){
    return SegmentModel.findById(req.params.id, function (err, segment) {
        if(!segment) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        
        delete req.body.__v;

        req.body.interval = req.body.interval._id;

        segment = extend(true, segment, req.body);
        
        segment.tests = req.body.tests;
        
        return segment.save(function (err) {
            if (!err) {
                log.info("segment updated");                
                return res.send({ status: 'OK', segment:segment });
            } else {
                if(err.name == 'ValidationError') {
                    res.statusCode = 400;
                    res.send({ error: 'Validation error' });
                } else {
                    res.statusCode = 500;
                    res.send({ error: 'Server error' });
                }
                log.error('Internal error(%d): %s',res.statusCode,err.message);
            }
        });
    });
};



exports.delete = function (req, res){
    return SegmentModel.findById(req.params.id, function (err, segment) {
        if(!segment) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        
		segment.pre('remove', function(next){
		    this.model('Card').update(
		        {_id: this.card_id}, 
		        {$pull: {segments: this._id}}, 
		        {multi: true},
		        next
		    );
		});
        
        return segment.remove(function (err) {
            if (!err) {
                log.info("segment removed");
                return res.send({ status: 'OK' });
            } else {
                res.statusCode = 500;
                log.error('Internal error(%d): %s',res.statusCode,err.message);
                return res.send({ error: 'Server error' });
            }
        });
    }); 
};