var IntervalModel = require('./../../libs/mongoose').IntervalModel;
var log = require('./../../libs/log')(module);

exports.readAll = function(req, res) {
    return IntervalModel.find({user_id:req.user._id}, function (err, intervals) {
        if (!err) {
            return res.send(intervals);
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
}

exports.create = function(req, res) {
    var interval = new IntervalModel({        
        user_id: req.body.user_id,
	    title: req.body.title,
	    intervals: req.body.intervals
    });

    interval.save(function (err) {
        if (!err) {
            log.info("interval created");
            return res.send({ status: 'OK', interval:interval });
        } else {
            console.log(err);
            if(err.name == 'ValidationError') {
                res.statusCode = 400;
                res.send({ error: 'Validation error' });
            } else {
                res.statusCode = 500;
                res.send({ error: 'Server error' });
            }
            log.error('Internal error(%d): %s',res.statusCode,err.message);
        }
    });
}

exports.read = function(req, res) {
    return IntervalModel.findById(req.params.id, function (err, interval) {
        if(!interval) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        if (!err) {
            return res.send({ status: 'OK', interval:interval });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
}

exports.update = function (req, res){
    return IntervalModel.findById(req.params.id, function (err, interval) {
        if(!interval) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }

        req.body.user_id ? interval.user_id = req.body.user_id : '';
        req.body.title ? interval.title = req.body.title : '';
        req.body.intervals ? interval.intervals = req.body.intervals : '';
        
        return interval.save(function (err) {
            if (!err) {
                log.info("interval updated");
                return res.send({ status: 'OK', interval:interval });
            } else {
                if(err.name == 'ValidationError') {
                    res.statusCode = 400;
                    res.send({ error: 'Validation error' });
                } else {
                    res.statusCode = 500;
                    res.send({ error: 'Server error' });
                }
                log.error('Internal error(%d): %s',res.statusCode,err.message);
            }
        });
    });
}

exports.delete = function (req, res){
    return IntervalModel.findById(req.params.id, function (err, interval) {
        if(!interval) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        return interval.remove(function (err) {
            if (!err) {
                log.info("interval removed");
                return res.send({ status: 'OK' });
            } else {
                res.statusCode = 500;
                log.error('Internal error(%d): %s',res.statusCode,err.message);
                return res.send({ error: 'Server error' });
            }
        });
    }); 
}