var TypeModel = require('./../../libs/mongoose').TypeModel;
var log = require('./../../libs/log')(module);
var extend = require('node.extend');

exports.readAll = function(req, res) {
    return TypeModel.find({user_id:req.user._id}, function (err, types) {
        if (!err) {
            return res.send(types);
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
};

exports.create = function(req, res) {
    var type = new TypeModel();
    
    type = extend(true, type, req.body);

    type.save(function (err) {
        if (!err) {
            log.info("type created");
            return res.send({ status: 'OK', type:type });
        } else {
            console.log(err);
            if(err.name == 'ValidationError') {
                res.statusCode = 400;
                res.send({ error: 'Validation error' });
            } else {
                res.statusCode = 500;
                res.send({ error: 'Server error' });
            }
            log.error('Internal error(%d): %s',res.statusCode,err.message);
        }
    });
};

exports.read = function(req, res) {
    return TypeModel.findById(req.params.id, function (err, type) {
        if(!type) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        if (!err) {
            return res.send({ status: 'OK', type:type });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
};

exports.update = function(req, res){
    return TypeModel.findById(req.params.id, function (err, type) {
        if(!type) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }

        type = extend(true, type, req.body);
        
        return type.save(function (err) {
            if (!err) {
                log.info("type updated");
                return res.send({ status: 'OK', type:type });
            } else {
                if(err.name == 'ValidationError') {
                    res.statusCode = 400;
                    res.send({ error: 'Validation error' });
                } else {
                    res.statusCode = 500;
                    res.send({ error: 'Server error' });
                }
                log.error('Internal error(%d): %s',res.statusCode,err.message);
            }
        });
    });
}

exports.delete = function (req, res){
    return TypeModel.findById(req.params.id, function (err, type) {
        if(!type) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        return type.remove(function (err) {
            if (!err) {
                log.info("type removed");
                return res.send({ status: 'OK' });
            } else {
                res.statusCode = 500;
                log.error('Internal error(%d): %s',res.statusCode,err.message);
                return res.send({ error: 'Server error' });
            }
        });
    }); 
}