var ClientModel = require('./../../libs/mongoose').ClientModel;
var log = require('./../../libs/log')(module);
var extend = require('node.extend');

exports.clientLogin = function(req, res) {
    // req.authInfo is set using the `info` argument supplied by
    // `BearerStrategy`.  It is typically used to indicate scope of the token,
    // and used in access control checks.  For illustrative purposes, this
    // example simply returns the scope in the response.
    res.json({ client_id: req.client.clientId, name: req.client.clientname, scope: req.authInfo.scope })
}

exports.readAll = function(req, res) {
    return ClientModel.find(function (err, clients) {
        if (!err) {
            return res.send(clients);
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
}

exports.create = function(req, res) {
    var client = new ClientModel();
    
    client = extend(true, client, req.body);

    client.save(function (err) {
        if (!err) {
            log.info("client created");
            return res.send({ status: 'OK', client:client });
        } else {
            console.log(err);
            if(err.name == 'ValidationError') {
                res.statusCode = 400;
                res.send({ error: 'Validation error' });
            } else {
                res.statusCode = 500;
                res.send({ error: 'Server error' });
            }
            log.error('Internal error(%d): %s',res.statusCode,err.message);
        }
    });
}

exports.read = function(req, res) {
    return ClientModel.findById(req.params.id, function (err, client) {
        if(!client) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        if (!err) {
            return res.send({ status: 'OK', client:client });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
}

exports.update = function(req, res){
    return ClientModel.findById(req.params.id, function (err, client) {
        if(!client) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }

        client = extend(true, client, req.body);
        
        return client.save(function (err) {
            if (!err) {
                log.info("client updated");
                return res.send({ status: 'OK', client:client });
            } else {
                if(err.name == 'ValidationError') {
                    res.statusCode = 400;
                    res.send({ error: 'Validation error' });
                } else {
                    res.statusCode = 500;
                    res.send({ error: 'Server error' });
                }
                log.error('Internal error(%d): %s',res.statusCode,err.message);
            }
        });
    });
}

exports.delete = function (req, res){
    return ClientModel.findById(req.params.id, function (err, client) {
        if(!client) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        return client.remove(function (err) {
            if (!err) {
                log.info("client removed");
                return res.send({ status: 'OK' });
            } else {
                res.statusCode = 500;
                log.error('Internal error(%d): %s',res.statusCode,err.message);
                return res.send({ error: 'Server error' });
            }
        });
    }); 
}