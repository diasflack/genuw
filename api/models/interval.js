var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var Interval = new Schema({
    user_id: { type: Schema.Types.ObjectId, ref: 'User', required: true },
    title: { type: String, required: true},
    intervals: { type: Array, required: true}
});

var IntervalModel = mongoose.model("Interval", Interval);

module.exports.IntervalModel = IntervalModel;

/*
 Basic intervals
 $scope.repeatIntervals = [

	  {
		"user_id": 0,
		"title": "Быстрое запоминание [сразу - 20 мин - 8ч - 24ч]",
		"intervals": [0,1200,28800,86400]
    },
    {
    	"user_id": 0,
		"title": "Нормальное запоминание [сразу - 20 мин - 24ч - 48ч - 72ч]",
		"intervals": [0,1200,86400,172800,259200]
    },
    {	
    	"user_id": 0,
		"title": "Глубокое запоминание [сразу - 20 мин - 48ч - 3 нед - 3 мес]",
		"intervals": [0,1200,172800,1814400,7776000]
    }
];

*/