var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var Type = new Schema({
	card_ids: [{ type: Schema.Types.ObjectId, ref: 'Card', required: true }],
	user_id: { type: Schema.Types.ObjectId, ref: 'User', required: true },
	title: { type: String, required: true },
	additionalFields: [{ type: String, required: false}]
});

var TypeModel = mongoose.model("Type", Type);

module.exports.TypeModel = TypeModel;

/*
 Basic type

  {
  	"card_ids": [],
	"user_id": 0,
	"title": "Статья"
}
*/
