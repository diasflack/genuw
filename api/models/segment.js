var mongoose = require("mongoose");
var Interval = require('./interval').IntervalModel.schema;

var Schema = mongoose.Schema;

var Test = new Schema({
    question: { type: String, required: false },
    answer: { type: String, required: false },
    evaluation: { type: Number, required: false }
});

var Segment = new Schema({
    user_id: { type: Schema.Types.ObjectId, ref: 'User', required: true },
    card_id: { type: Schema.Types.ObjectId, ref: 'Card', required: true },

    title: { type: String, required: true },
    timer: { type: String, required: true },
    started: { type: Date, required: true },

	repeated: { type: Number, required: true },
	interval: { type: Schema.Types.ObjectId, ref: 'Interval', required: true },
    lastRepeat: { type: Date, required: true },
    nextRepeat: { type: Date, required: true },

    stopPoint: { type: String, required: false },
    tests: [Test],
    rememberLevel: { type: String, required: false }
});

var SegmentModel = mongoose.model("Segment", Segment);

module.exports.SegmentModel = SegmentModel;