var mongoose = require("mongoose");
var deepPopulate = require('mongoose-deep-populate');

var Schema = mongoose.Schema;

var AdditionalField = new Schema({
	title: { type: String, required: false },
	value: { type: String, required: false }
});

var Card = new Schema({
	user_id: { type: Schema.Types.ObjectId, ref: 'User', required: true },
	segments: [{ type: Schema.Types.ObjectId, ref: 'Segment', required: true }],
	rememberLevel:  { type: String, required: true },
	started: { type: Date, required: true },
	title: { type: String, required: true },
	type: { type: Schema.Types.ObjectId, ref: 'Type', required: true },
	additionalFields: [AdditionalField],
	tags: { type: Array }
});

Card.plugin(deepPopulate);

var CardModel = mongoose.model("Card", Card);

module.exports.CardModel = CardModel;

/*
 Basic card

  {
	"user_id": 0,
	"title": "Карта 1",
	"type": "Статья",
	"segments": []
}
*/
