describe('MindHauz login page', function() {
    var loginInput = element(by.model('loginCtrl.login.email')),
        passwordInput = element(by.model('loginCtrl.login.password')),
        loginButton = element(by.buttonText('Log In')),
        registerButton = element(by.buttonText('One Click Registration')),
        emailError = element(by.css('[ng-message="email"]')),
        passwordError = element(by.css('[ng-message="minlength"]')),
        loginError = element(by.model('loginCtrl.loginError'));


    beforeEach(function(){
        browser.get(browser.params.testHost);
    });

    it('should load page', function() {
        expect(browser.getTitle()).toEqual('Ultimate learning tool');
    });

    it('should disable buttons if inputs are empty', function () {
        expect(loginButton.isEnabled()).toBe(false);
        expect(loginButton.isEnabled()).toBe(false);
    });

    it('should show error if invalid e-mail entered', function() {
        loginInput.sendKeys('123456');

        expect(emailError.isDisplayed()).toBe(true);
        expect(emailError.getText()).toEqual('Enter valid email address.');
    });

    it('should hide error if valid e-mail entered', function() {
        loginInput.sendKeys('123456');
        loginInput.sendKeys('@mail.ru');

        expect(emailError.isPresent()).toBe(false);
    });

    it('should show error if invalid password length entered', function() {
        passwordInput.sendKeys('12345');

        expect(passwordError.isDisplayed()).toBe(true);
        expect(passwordError.getText()).toEqual('Minimum password length is 6 symbols.');
    });

    it('should hide error if valid e-mail entered', function() {
        passwordInput.sendKeys('12345');
        passwordInput.sendKeys('6');

        expect(passwordError.isPresent()).toBe(false);
    });

    it('should show login error if invalid credentials entered', function() {
        loginInput.sendKeys('demo@demo.ru');
        passwordInput.sendKeys('123456');

        loginButton.click();

        expect(loginError.isPresent()).toBe(true);
        expect(loginError.getText()).toBe('Invalid login/password combination');
    });

    it('should show register error if user exist', function() {
        loginInput.sendKeys('demo@demo.ru');
        passwordInput.sendKeys('123456');

        registerButton.click();

        expect(loginError.isPresent()).toBe(true);
        expect(loginError.getText()).toBe('User already exist');
    });

    it('should login if credentials right', function() {
        loginInput.sendKeys(browser.params.testLogin.login);
        passwordInput.sendKeys(browser.params.testLogin.pass);

        loginButton.click();

        expect(browser.getCurrentUrl()).toBe(browser.params.testHost + browser.params.pages.home);
    });

});
