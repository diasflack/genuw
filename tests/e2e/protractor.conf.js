exports.config = {
    framework: 'jasmine2',

    seleniumAddress: 'http://localhost:4444/wd/hub',

    capabilities: {
        'browserName': 'chrome'
    },

    specs: ['spec/*_spec.js'],

    jasmineNodeOpts: {
        showColors: true // Use colors in the command line report.
    },

    params: {
        testLogin: {
            login: 'test@test.ru',
            pass: 'testtest'
        },
        testHost: 'http://localhost:5000/',
        pages: {
            home: '#/home'
        }
    }
};

